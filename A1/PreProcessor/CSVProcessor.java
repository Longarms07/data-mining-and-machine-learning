import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;


public class CSVProcessor
{
    public void CSVProcessor(){
    }
    
    public ArrayList<String[]> ReadNonCsv(File toProcess){
        ArrayList<String[]> data = new ArrayList<String[]>();
            try{
                Scanner scanner = new Scanner(toProcess);
                //int counter = 0;
                while(scanner.hasNextLine()/*&& counter < 200001*/){
                    String line = scanner.nextLine();
                    String[] temp = line.split(" ");
                    //counter++;
                    //System.out.println(temp.length);
                    data.add(temp); 
                    
                }
            }
            catch(FileNotFoundException e){
                e.printStackTrace();
            }
            System.out.println("File read!");
            return data;
            
    }
    
    public ArrayList<String[]> Read(File toProcess){
        ArrayList<String[]> data = new ArrayList<String[]>();
        try{
            Scanner scanner = new Scanner(toProcess);
            //int counter = 0;
            while(scanner.hasNextLine()/*&& counter < 200001*/){
                String line = scanner.nextLine();
                String[] temp = line.split(",");
                //counter++;
                //System.out.println(temp.length);
                data.add(temp); 
                
            }
        }
        catch(FileNotFoundException e){
            e.printStackTrace();
        }
        System.out.println("File read!");
        return data;
        
    }
    
    public <T> void Write(ArrayList<T[]> data, String filename, boolean Header){
        FileWriter newFile = null;
        try{
            newFile = new FileWriter(filename);
            
            
            for(int i = 0;i<data.size(); i++){ //each value is a line
                T[] current = data.get(i);
                if(i == 0 && Header == false){
                i = 2;
                }
                //System.out.println(current.length);
                if(current.length != 1){
                    for(int j = 0; j<current.length;j++){ //add each value and seperate by commas
                        /*if(i == 0){
                        *    System.out.println(current[j].toString());
                        }*/
                        newFile.append(current[j].toString());
                        if(j < current.length-1){
                            newFile.append(",");
                        }
                    }
                }
                newFile.append("\n"); //make new line
            }
        }
        catch (Exception e){
            System.out.println("Error writing file!");
            e.printStackTrace();
        }
        finally{
            try{
                
                newFile.close();
                System.out.println("New file Written: " + filename + "!");
            }
            catch (Exception e){
            System.out.println("Error closing filewriter!");
            e.printStackTrace();
            }
        }
    }
    
    public ArrayList<String[]> remove4Columns(ArrayList<String[]> data, int c1, int c2, 
        int c3, int c4){
        ArrayList<String[]> newData = new ArrayList<String[]>();
        String[] current;
        for(int i=0;i<data.size();i++){
            current = data.get(i);
            //System.out.println(current.length);
            if(current.length>4){
                String[] temp = new String[current.length-4];
                int offset = 0;
                for(int j = 0; j<temp.length;j++){
                    
                    while(offset == c1 || offset == c2 || offset == c3 || offset == c4){
                        offset++;
                    }
                    //if(i==0)System.out.println(current[offset]);
                    temp[j] = current[offset];
                    offset++;
                }
                newData.add(temp);
            }
        }
        
        return newData;
    }
    
    
    public ArrayList<String[]> removeColumns(ArrayList<String[]> data, ArrayList<Integer> columnNum){
        //make a way to remove any column based on the list
        ArrayList<String[]> newData = new ArrayList<String[]>();
        ArrayList<Integer> cN = columnNum;
        int target = cN.size(); 
        String[] current;
        for(int i=0;i<data.size();i++){
            current = data.get(i);
            //System.out.println(current.length);
            if(current.length>target){
                String[] temp = new String[current.length-target];
                int offset = 0;
                for(int j = 0; j<temp.length;j++){
                    
                    for(int k = 0; k<target; k++){
                        //System.out.println(k);
                        if(cN.get(k) == offset) offset++; 
                        //System.out.println(offset);
                    }
                    
                    if(i==0)System.out.println(current[offset]);
                    
                    temp[j] = current[offset];
                    offset++;
                }
                newData.add(temp);
            }
        }
        
        return newData;
    
    }
    
    public ArrayList<String[]> removeHeader(ArrayList<String[]> datas){
        ArrayList<String[]> data = new ArrayList(datas);
        ArrayList<String[]> newData = new ArrayList<String[]>();
        String[] current;
        for(int i=2;i<data.size();i++){
            current = data.get(i);
            //System.out.println(current.length);
            
                String[] temp = new String[current.length];
                
                for(int j = 0; j<temp.length;j++){
                    temp[j] = current[j];
                }
                newData.add(temp);
            
        }
        
        
        return newData;
    }
    //split columns into two
    public ArrayList<String[]> splitColumns(ArrayList<String[]> datas, ArrayList<Integer> columnNum, boolean HeaderPresent){
        //make a way to remove any column based on the list
        ArrayList<String[]> newData = new ArrayList<String[]>();
        ArrayList<String[]> data = new ArrayList(datas);
        if(HeaderPresent == true) data = this.removeHeader(data);
        
        ArrayList<Integer> cN = columnNum;
        int target = cN.size(); 
        String[] current;
        for(int i=0;i<data.size();i++){
            current = data.get(i);
            
            //System.out.println(current.length);
            if(current.length>1){
                String[] temp = new String[current.length+target];
                int offset = 0;
                for(int j = 0; j<current.length;j++){
                boolean entered = false;    
                //System.out.println(current[j]);
                    for(int k = 0; k<target; k++){
                        if(cN.get(k).equals(j)){
                            //System.out.println("k = " + cN.get(k) + " J = "+j);
                            String str = current[j];
                            //System.out.println(str);
                            String[] splitLine = str.split(" ");
                            if(splitLine.length > 1){
                            temp[offset] = splitLine[0];
                            //System.out.println("Offset = " + offset);
                            offset=offset+1;
                            //System.out.println("Offset = " + offset);
                            temp[offset] = splitLine[1];
                            entered = true;
                        }
                    }
                    
                }
                //if(i==0)System.out.println(current[offset]);
                    //System.out.println("Offset = " + offset + " J = " + j);
                    if(entered == false) temp[offset] = current[j];
                    offset++;
            }
            newData.add(temp);
        }else{newData.add(current);}
    }
        
        return newData;
    
    }
    
    public ArrayList<String[]> randomSelect(ArrayList<String[]> datas, int number){
        ArrayList<String[]> Rand = new ArrayList<String[]>();
        ArrayList<String[]> data = new ArrayList<String[]>(datas);
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        
        Rand.add(data.get(0));
        Rand.add(data.get(1));
        while(Rand.size()<number+2){
            int value = (int)(Math.random()*data.size()-2)+2;
            while(numbers.contains(value)){
                value = (int)(Math.random()*data.size()-2)+2;
            }
            //System.out.println(value);
            numbers.add(value);
            Rand.add(data.get(value));
        
        }
       
        return Rand;
    }
    
    
    
    
}
